from pprint import pprint

gpu_node_list = ['bender', 'clamps', 'roberto']
cpu_node_list = ['bender', 'clamps', 'flexo', 'perceptron', 'roberto']
all_hail_dgx = ['calculon']
dump_to_file = 'ohmyaliases.txt' 

max_no_of_gpus = 4
all_aliases = []

for no_of_gpus in range(max_no_of_gpus):
	all_aliases.append('alias accio_{}_gpu_anywhere="srun -p gpu --gres=gpu:{} --pty /bin/bash"'\
		.format(no_of_gpus+1, no_of_gpus+1))

for gpu_node_idx in range(len(gpu_node_list)):
	for no_of_gpus in range(max_no_of_gpus):
		all_aliases.append('alias accio_{}_gpu_on_{}="srun -p gpu --gres=gpu:{} -w {} --pty /bin/bash"'\
			.format(no_of_gpus+1, gpu_node_list[gpu_node_idx], no_of_gpus+1, gpu_node_list[gpu_node_idx]))

all_aliases.append('alias accio_calculon="srun -p dgx  --pty /bin/bash"')

pprint(all_aliases)

f = open(dump_to_file, 'w')
for each_line in all_aliases:
	f.write("%s\n" % each_line)
f.close()